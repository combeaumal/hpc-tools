#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include "mkl_lapacke.h"

double *generate_matrix(int size)
{
   int i;
    int tmp;
        int j ;
    double *matrix = (double *)malloc(sizeof(double) * size * size);
    srand(1);
  // we want to generate a diagonally dominant matrix 
    for (i = 0; i < size; i++)
    {

        tmp = 0;

        for(j = 0; j<size; j++)
        {
            if(i == j)
            {
                matrix[i*size+j] = 0;
            }
            else
            {
                matrix[i*size+j] = rand() % 100;
                tmp += matrix[i*size+j];
            }

            if(j == (size-1))
            {
                matrix[i*size+i] = tmp + rand() % 100;
            }
        }
    }
    return matrix;
}



void print_matrix(const char *name, double *matrix, int size)
{
    int i, j;
    printf("matrix: %s \n", matrix);

    for (i = 0; i < size; i++)
    {
            for (j = 0; j < size; j++)
            {
                printf("%f ", matrix[i * size + j]);
            }
            printf("\n");
    }
}

int check_result(double *bref, double *b, int size) {
    int i;
    for(i=0;i<size*size;i++) {
        if (bref[i]!=b[i]) return 0;
    }
    return 1;
}



double* diagMatrix(double *matrix,int size){
    double *diag = (double *)malloc(sizeof(double) * size * size);
    for (int i = 0 ; i < size*size ; i++)
    {
        diag[i] = 0;
    }

    for(int i = 0; i < size; i++)
    {
            diag[i*size+i] = matrix[i*size+i];
    }
    return diag;
}


double* triangularLMatrix(double *matrix,int size){


    double *triangularL = (double *)malloc(sizeof(double) * size * size);

    for (int i = 0; i < size*size; ++i)
    {
        triangularL[i]=matrix[i];
    }
    for(int k = 0; k < size; k++)
    {
        for (int i = k+1; i < size; i++)
        {
            triangularL[k*size+i]=0;
        }     
    }
    return triangularL;
}
 

double* triangularUMatrix(double *matrix,int size){
    double *triangularU = (double *)malloc(sizeof(double) * size * size);
    
    for (int i = 0; i < size*size; ++i)
    {
        triangularU[i]=matrix[i];
    }

    for(int k = 0; k < size; k++)
    {
        for (int i = k+1; i < size; i++)
        {
            triangularU[i*size+k]=0;
        }     
    }
    return triangularU;
}

 
double* invD(double *matrix, int size){
    double *inverse = (double *)malloc(sizeof(double) * size * size);
    for(int i = 0; i < size; i++)
    {
            inverse[i*size+i] = 1/ matrix[i*size+i];
    }
    return inverse;
}

double* oppose(double *matrix, int size) {
    double *opp = (double *)malloc(sizeof(double) * size * size);
    for (int i = 0 ; i < size*size ; i++)
    {
        opp[i] = - matrix[i] ;
    }
   /* for(int i = 0; i < size; i++)
    {
        opp[i*size+i] = - matrix[i*size+i] ;
    }*/
    return opp;
}

double* Somme(double *matrix1, double *matrix2 , int size){
    double *sum = (double *)malloc(sizeof(double) * size * size);
    for (int i = 0 ; i < size*size ; i++)
    {
        sum[i] = matrix1[i] + matrix2[i];
    }
   /* for(int i = 0; i < size; i++)
    {
            sum[i*size+i] = matrix1[i*size+i] + matrix2[i*size+i];
    }*/
    return sum;
}

double* multMatrix(double *matrix1, double *matrix2,int size)
{
    double *mult = (double *)malloc(sizeof(double) * size * size);

    for(int i=0;i<size;i++)
    {
        for (int j = 0; j < size; j++)
        {
            mult[i*size+j]=0;
            for (int k = 0; k < size; k++)
            {
                mult[i*size+j]+=matrix1[i*size+k]*matrix2[k*size+j];
            }
        }
    }
    return mult;
}


//int my_dgesv(int n, int nrhs, double *a, int lda, int *ipiv, double *b, int ldb) {

    //Replace with your implementation
 //   LAPACKE_dgesv(LAPACK_ROW_MAJOR, n, nrhs, a, lda, ipiv, b, ldb);
    
//}

double* jacobi(double *matrix1, double *matrix2 , int nmaxit , double eps , int size)
{
    double *X = (double *)malloc(sizeof(double) * size * size);
    double *D = (double *)malloc(sizeof(double) * size * size);
    double *F = (double *)malloc(sizeof(double) * size * size);
    double *E = (double *)malloc(sizeof(double) * size * size);
    double *tmp1 = (double *)malloc(sizeof(double) * size * size);
    double *tmp2 = (double *)malloc(sizeof(double) * size * size);
    double *tmp3 = (double *)malloc(sizeof(double) * size * size);
    double *tmp4 = (double *)malloc(sizeof(double) * size * size);



    D = diagMatrix(matrix1,size);
    E = Somme( oppose(triangularUMatrix(matrix1, size),size), D, size);
    F = Somme( oppose(triangularLMatrix(matrix1, size),size), D, size);

    
    for (int i = 0; i < size*size; ++i)
    {
        X[i]= 0;
    }
   
    for (int k = 1; k < nmaxit; k++)
    {   

    tmp1 = multMatrix(invD(D,size),matrix2,size);
           
    tmp2 = Somme(E, F, size);
          
    tmp3 = multMatrix(invD(D,size),tmp2,size);
          
    tmp4 = multMatrix(tmp3,X,size);
           
    X = Somme(tmp4,tmp1,size);
          
       }
 
    return X;
 }


    void main(int argc, char *argv[])
    {

        int size = atoi(argv[1]);
       int nmaxit = 100*size;
       int eps = 0.01/size;
       double *a = (double *)malloc(sizeof(double) * size * size);
    
       double *b = (double *)malloc(sizeof(double) * size * size);
       double *aref = (double *)malloc(sizeof(double) * size * size);
    
       double *bref = (double *)malloc(sizeof(double) * size * size);
    
       double *X = (double *)malloc(sizeof(double) * size * size);


        a = generate_matrix(size);
        aref = generate_matrix(size);        
        b = generate_matrix(size);
        bref = generate_matrix(size);

        // Using MKL to solve the system
        MKL_INT n = size, nrhs = size, lda = size, ldb = size, info;
        MKL_INT *ipiv = (MKL_INT *)malloc(sizeof(MKL_INT)*size);

        clock_t tStart = clock();
        info = LAPACKE_dgesv(LAPACK_ROW_MAJOR, n, nrhs, aref, lda, ipiv, bref, ldb);
        printf("Time taken by MKL: %.2fs\n", (double)(clock() - tStart) / CLOCKS_PER_SEC);       

        tStart = clock();     
        X = jacobi(a, b , nmaxit , eps , size);
        printf("Time taken by my implementation: %.2fs\n", (double)(clock() - tStart) / CLOCKS_PER_SEC);
        
        if (check_result(bref,X,size)==1)
            printf("Result is ok!\n");
        else    
            printf("Result is wrong!\n");
    }
